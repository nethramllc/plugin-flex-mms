# Plugin-Flex-MMS

This plugin adds MMS support to flex webchat.

Note: Twilio officially doesnt support MMS in flex webchat.

## Setup

1. Create a function in your twilio account and copy the code inside
   functions/update-mms/update-mms.js into it.

2. Give the function url in the callback field of your proxy service.

   Note: More information about setting up function is in functions/update-mms/README.md

3. Make sure you have the following dependencies installed in your local
   environment:-
   * Node.js
   * npm
   * twilio-cli

4. Clone the repository.
   `git clone https://github.com/thameemkj/plugin-flex-mms.git`

5. Change the directory to the cloned repository.
   `cd plugin-flex-mms`

6. Run `npm install`

7. Create a copy of public/appConfig.example.js with name appConfig.js in the same directory (public/appConfig.js)
   `cp public/appConfig.example.js public/appConfig.js`

8. Now run `twilio flex:plugins:start`

