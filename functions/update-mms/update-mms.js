const Urllib = require("urllib");

exports.handler = async function(context, event, callback) {
  let inboundResourceSid = event.inboundResourceSid;

  try{
    if(inboundResourceSid.startsWith("MM")){
      const client = context.getTwilioClient();
      let proxyServiceSid = event.interactionServiceSid;
      let sessionSid = event.interactionSessionSid;
      let messageSid = event.outboundResourceSid;
      let chatServiceSid = context.CHAT_SERVICE_SID;
      let mediaUrl = event.interactionMediaUrl0;
      let session = await client.proxy
        .services(proxyServiceSid)
        .sessions(sessionSid)
        .fetch();
      
      let channelSid = session.uniqueName.split('.')[0];

      Urllib.request(
        mediaUrl,
        {
          method: 'HEAD',
          dataType: "json",
          followRedirect: true
        },
        async function(error, data, response){
          if(error){
            callback(error);
          }else {
            let statusCode = response['statusCode'];
            if(statusCode === 200){
              let contentType = response.headers['content-type'].split("/")[0];
              let attributes = {
                "proxied": true,
                "mediaUrl": mediaUrl,
                "mediaType": contentType
              };
              
              let chatMessageResource = await client.chat
                .services(chatServiceSid)
                .channels(channelSid)
                .messages(messageSid)
                .update({
                  attributes: JSON.stringify(attributes)
              });
              callback(null, chatMessageResource);
            }else{
              callback(statusCode,"Media not available");
            }
          }
        }
      );
    }else{
      let chatMessageResource = await client.chat
        .services(chatServiceSid)
        .channels(channelSid)
        .messages(messageSid)
        .fetch();
      callback(null,chatMessageResource);
    }
  }catch(error){
    callback(error);
  }
};
