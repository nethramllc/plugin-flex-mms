# Update-MMS

This function is invoked when an SMS or MMS is recieved by a proxy service.
This function takes the media url from the parameters recieved and updates 
attributes property of the chat resource with the media url and its related 
properties. The rest is handled by the flex-mms plugin.

## Environment variables

This function requires the following environment variables:-
*   ACCOUNT_SID
*   AUTH_TOKEN
*   CHAT_SERVICE_SID

## Dependencies

This function requires the following node packages:-
*   urllib (tested version: 2.37.4)
