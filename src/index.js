import * as FlexPlugin from 'flex-plugin';

import FlexMmsPlugin from './FlexMmsPlugin';

FlexPlugin.loadPlugin(FlexMmsPlugin);
