import React from 'react';
import { VERSION } from '@twilio/flex-ui';
import { FlexPlugin } from 'flex-plugin';

import MediaPreviewComponent from './components/MediaPreviewComponent';

const PLUGIN_NAME = 'FlexMmsPlugin';

export default class FlexMmsPlugin extends FlexPlugin {
  constructor() {
    super(PLUGIN_NAME);
  }

  /**
   * This code is run when your plugin is being started
   * Use this to modify any UI components or attach to the actions framework
   *
   * @param flex { typeof import('@twilio/flex-ui') }
   * @param manager { import('@twilio/flex-ui').Manager }
   */
  async init(flex, manager) {
    
    /**
     * Replacing default chat-bubble with custom react component
     */
    flex.MessageBubble.Content.replace(
      <MediaPreviewComponent key="body" />,
      {
        if : props => props.message.source.media === null 
      }
    );
    
  }
}
