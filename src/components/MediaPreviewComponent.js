import "./MediaPreviewComponent.css";

export default function MediaPreviewComponent(props) {

  let messageFrom = props.member.friendlyName;
  let messageIndex = props.message.index; 
  let messageBody = props.message.source.body;
  let mediaUrl = props.message.source.attributes.mediaUrl;
  let mediaType = props.message.source.attributes.mediaType;

  if (mediaType === "image") {
    return (
      <div className="MediaPreviewComponentContainer">
        <button className="MediaPreviewButton" onClick={() => window.open(mediaUrl)}>
          <p className="MediaMessageFrom"> {messageIndex == 0?messageFrom:null} </p>
          <img className="MediaPreview" src={mediaUrl} />
          <p className="MediaMessageBody">{messageBody} </p>
        </button>
      </div>
    );
  }else if (mediaType === "video") {
    return (
      <div className="MediaPreviewComponentContainer">
        <button className="MediaPreviewButton" onClick={() => window.open(mediaUrl)}>
          <p className="MediaMessageFrom"> {messageIndex == 0?messageFrom:null} </p>
          <video className="MediaPreview" src={mediaUrl} controls autoplay muted />
          <p className="MediaMessageBody">{messageBody} </p>
        </button>
      </div>
    );
  } else {
    return (
      <div className="MediaPreviewComponentContainer">
        <button className="MediaPreviewButton" onClick={() => window.open(mediaUrl)}>
          <p className="MediaMessageFrom"> {messageIndex == 0?messageFrom:null} </p>
          <p className="UnknownMediaPreview">
            Unsupported file type. Please click to open the link in a new tab
          </p>
          <p className="MediaMessageBody">{messageBody} </p>
        </button>
      </div>
    );
  }
}
